<?php


// Check to see if user wants to user for all galleries or use shortcode.


setup_zfoundation_gallery_shortcode();

function setup_zfoundation_gallery_shortcode() {

	//First remove the standard wordpress gallery shortcode action
	remove_shortcode('gallery', 'gallery_shortcode' );

	//Add our foundation clearing gallery shortcode action here
	add_shortcode('gallery', 'zfoundation_gallery_shortcode' );

}



// Load needed Foundation CSS, JS if not included in theme.



function zfoundation_gallery_shortcode( $attr ) {
    $post = get_post();

    static $instance = 0;
    $instance++;

    if ( ! empty( $attr['ids'] ) ) {
        // 'ids' is explicitly ordered, unless you specify otherwise.
        if ( empty( $attr['orderby'] ) ) {
            $attr['orderby'] = 'post__in';
        }
        $attr['include'] = $attr['ids'];
    }

    /**
     * Filter the default gallery shortcode output.
     *
     * If the filtered output isn't empty, it will be used instead of generating
     * the default gallery template.
     *
     * @since 2.5.0
     *
     * @see gallery_shortcode()
     *
     * @param string $output The gallery output. Default empty.
     * @param array  $attr   Attributes of the gallery shortcode.
     */
    $output = apply_filters( 'post_gallery', '', $attr );
    if ( $output != '' ) {
        return $output;
    }

    $html5 = current_theme_supports( 'html5', 'gallery' );
    $atts = shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post ? $post->ID : 0,
        'itemtag'    => $html5 ? 'figure'     : 'dl',
        'icontag'    => $html5 ? 'div'        : 'dt',
        'captiontag' => $html5 ? 'figcaption' : 'dd',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => '',
        'link'       => '',
        'captions'	 => false,
        'clearing'   => true
    ), $attr, 'gallery' );

    $id = intval( $atts['id'] );

    if ( ! empty( $atts['include'] ) ) {
        $_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( ! empty( $atts['exclude'] ) ) {
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    } else {
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    }

    if ( empty( $attachments ) ) {
        return '';
    }

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment ) {
            $output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
        }
        return $output;
    }

    $itemtag = tag_escape( $atts['itemtag'] );
    $captiontag = tag_escape( $atts['captiontag'] );
    $icontag = tag_escape( $atts['icontag'] );
    $valid_tags = wp_kses_allowed_html( 'post' );
    if ( ! isset( $valid_tags[ $itemtag ] ) ) {
        $itemtag = 'dl';
    }
    if ( ! isset( $valid_tags[ $captiontag ] ) ) {
        $captiontag = 'dd';
    }
    if ( ! isset( $valid_tags[ $icontag ] ) ) {
        $icontag = 'dt';
    }

    $columns = intval( $atts['columns'] );
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $size_class = sanitize_html_class( $atts['size'] );

    // Enable Clearing?
    if ($atts['clearing'] == true) {
	    $enable_clearing = 'data-clearing';
	} else {
		$enable_clearing = '';
	}

    // Create the grid
	switch( $columns ) {
		case 1:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-1' $enable_clearing>";
			break;
		case 2:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-1 medium-block-grid-2' $enable_clearing>";
			break;
		case 3:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-1 medium-block-grid-2 large-block-grid-3' $enable_clearing>";
			break;
		case 5:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-2 medium-block-grid-3 large-block-grid-5' $enable_clearing>";
			break;
		case 6:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-2 medium-block-grid-4 large-block-grid-6' $enable_clearing>";
			break;
		default:
			$gallery_ul = "<ul id='$selector' class='gallery galleryid-{$id} gallery-size-{$size_class} clearing-thumbs small-block-grid-2 medium-block-grid-3 large-block-grid-4' $enable_clearing>";
			break;
	}

    $output = apply_filters( 'gallery_style', $gallery_ul );

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {

		$output .= '<li>';

        $attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
        /*
        if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
            $image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
        } elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
            $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
        } else {
            $image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
        }
        */

        if ($atts['clearing'] == false) { // just image, no link
	        $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
        } else { // link with image
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
        }

        $image_meta  = wp_get_attachment_metadata( $id );

        $orientation = '';
        if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
            $orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
        }

        // caption
        $caption_text = wptexturize($attachment->post_excerpt);
        $image_output = str_replace('<img', "<img data-caption='{$caption_text}'", $image_output);

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "
            <{$icontag} class='gallery-icon {$orientation}'>
                $image_output
            </{$icontag}>";

        if ( $captiontag && trim($attachment->post_excerpt) && $atts['captions'] == true ) {
            $output .= "
                <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
                " . $caption_text . "
                </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";

        $output .= '</li>';
    }

    $output .= "
        </ul>\n"; // end gallery

    return $output;
}